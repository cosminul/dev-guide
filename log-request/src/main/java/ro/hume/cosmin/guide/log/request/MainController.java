package ro.hume.cosmin.guide.log.request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/")
public class MainController {

	private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String showPage() {
		LOG.info("Showing main page.");
		return "main";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String postData(@RequestParam("age") int age, @RequestParam("password") String password) {
		return "redirect:/main";
	}
}
