package ro.hume.cosmin.guide.log.request;

import javax.servlet.Filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

	@Bean
	public Filter logFilter() {
		PostRequestLoggingFilter filter = new PostRequestLoggingFilter();
		filter.setIncludeQueryString(true);
		filter.setIncludePayload(true);
		filter.setMaxPayloadLength(1024);
		return filter;
	}
}
