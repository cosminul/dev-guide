package ro.hume.cosmin.guide.log.request;

public interface PasswordRemover {

	String removePassword(String uri);
}
