# Log Request

Example for logging all POST requests in a Spring MVC application.

Explained in [an article](https://cosmin.hume.ro/2018/09/logging-post-requests-in-a-spring-web-application/).
