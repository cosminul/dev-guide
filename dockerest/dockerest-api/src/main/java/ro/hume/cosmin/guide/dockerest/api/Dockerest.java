package ro.hume.cosmin.guide.dockerest.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dockerest {

	public static void main(String[] args) {
		SpringApplication.run(Dockerest.class, args);
	}
}
