package com.staticdot.guide.selenium.hello;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class HelloSelenium {

	public static void main(String[] args) {

		DesiredCapabilities firefoxCapability = DesiredCapabilities.firefox();
		WebDriver driver = null;
		try {
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), firefoxCapability);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		driver.get("https://google.com/");
		System.out.println("Page title is: " + driver.getTitle());
		driver.quit();
	}
}
